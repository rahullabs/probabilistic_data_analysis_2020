# Probabilistic data analysis: research seminar 2020

## Schedule

The following links list study material for each session (available one week in advance for each session).

  * [Session 1](session1.md) (March 20, 2020 / Leo Lahti)
  * [Session 2](session2.md) (April 3, 2020 / Ville Laitinen)
  * [Session 3](session3.md) (April 17, 2020 / Aaro Salosensaari)
  * [Session 4](session4.md) (May 1, 2020 / Moein Khalighi)
  * [Session 5](session5.md) (May 15, 2020 / Felix Vaura)
  * [Session 6](session6.md) (May 29, 2020 / Guilhem Sommeria-Klein)
  * [Session 7](session6.md) (TBA / Iiro)  
  * .. activities during the summer months can be discussed


## Participation

Each participant will:

  1. study the material and do the exercises in advance before every session
  1. participate actively in the sessions, and
  1. in his/her turn choose and announce study material one week before the next session

For other practical information, see [README](README.md) (time, location, contact details).

## Source material

To prepare a session on your own turn, check the following [material](material.md).

